import Photon from '@generated/photon'

const photon = new Photon()

async function main() {
  const user = await photon.users.create({
    data: {
      name: 'Jane',
      email: 'jane@prisma.io',
      password: 'jane',
      posts: {
        create: [
          {
            title: 'Subscribe to GraphQL Weekly for community news',
            content: 'https://graphqlweekly.com/',
          },
          {
            title: 'Follow Prisma on Twitter',
            content: 'https://twitter.com/prisma',
          },
        ],
      },
    },
  })
  console.log({ user })
}

main()
  .catch(e => console.error(e))
  .finally(async () => {
    await photon.disconnect()
  })
