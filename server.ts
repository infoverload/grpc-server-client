const PROTO_PATH = __dirname + '/service.proto'

import Photon from '@generated/photon'
import * as protoLoader from '@grpc/proto-loader'
import * as grpc from 'grpc'

const photon = new Photon()

const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
})
const { protoDescriptor } = grpc.loadPackageDefinition(packageDefinition) as any

const showFeed = async(call: any, callback: any) => {
  const feed = await photon.posts.findMany()
  callback(null, { feed })
}

const getPost = async(call: any, callback: any) => {
  const { id } = call.request
  const post = await photon.posts.findOne({
    where: {
      id,
    },
  })
  callback(null, { post })
}

const signupUser = async(call: any, callback: any) => {
  const { name, email, password } = call.request
  try {
    const newUser = await photon.users.create({
      data: {
        name,
        email,
        password,
      },
    })
    callback(null, newUser)
  } catch (e) {
    callback(e, null)
  }
}

const createPost = async(call: any, callback: any) => {
  const { title, content, author } = call.request
  try {
    const newPost = await photon.posts.create({
      data: {
        title,
        content,
        //author,
      },
    })
    callback(null, newPost)
  } catch (e) {
    callback(e, null)
  }
}

const server = new grpc.Server()
server.addService(protoDescriptor.Blog.service, {
  showFeed,
  getPost,
  signupUser,
  createPost,
})
server.bind('0.0.0.0:50051', grpc.ServerCredentials.createInsecure())
const message = `
The gRPC server is being started on ${`0.0.0.0:50051`}.

You can invoke the client script by running: ${`$ npm run client`}.

Use ${`BloomRPC`} if you prefer a GUI client (download: ${`https://github.com/uw-labs/bloomrpc`}).
`
console.log(message)
server.start()
