const PROTO_PATH = __dirname + '/service.proto'

import * as protoLoader from '@grpc/proto-loader'
import * as grpc from 'grpc'
const packageDefinition = protoLoader.loadSync(PROTO_PATH, {
  keepCase: true,
  longs: String,
  enums: String,
  defaults: true,
  oneofs: true,
})
const { protoDescriptor } = grpc.loadPackageDefinition(packageDefinition) as any

function main() {
  const client = new protoDescriptor.Blog(
    'localhost:50051',
    grpc.credentials.createInsecure(),
  )

  // show feed
  client.showFeed({}, (err: any, response: any) => {
    if (err) {
      console.error(err)
      return
    }
    console.log(response)
  })

  // get post
  const id = ''
  client.getPost(id, (err: any, response: any) => {
    if (err) {
      console.error(err)
      return
    }
    console.log(response)
  })

  // create new user
  const user = {
    name: 'Jack',
    email: 'jack@prisma.io',
    password: 'jack',
  }
  client.signupUser(user, (err: any, response: any) => {
    if (err) {
      console.error(err)
      return
    }
    console.log(response)
  })

  // create new post
  const post = {
    title: 'Hello World',
    content: 'This is my first post',
    user: {
      name: 'Jane',
      email: 'jane@prisma.io',
    }
  }
  client.createPost(post, (err: any, response: any) => {
    if (err) {
      console.error(err)
      return
    }
    console.log(response)
  })
}

main()
