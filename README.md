# gRPC Server and Client in TypeScript

This is a basic implementation of a **gRPC Server and Client** with [Prisma 2](https://www.prisma.io/).

It uses [Lift](https://lift.prisma.io) for database migrations and [Photon JS](https://photonjs.prisma.io) for database access.

## How to use

### 1. Download files & install dependencies

Clone the repository:

```sh
git clone git@github.com:infoverload/prisma2-grpc.git
```

Install Node dependencies:

```sh
cd prisma2-grpc
npm install
```

### 2. Install the Prisma 2 CLI

To run the example, you need the [Prisma 2 CLI](https://github.com/prisma/prisma2/blob/master/docs/prisma-2-cli.md):

```sh
npm install -g prisma2
```

### 3. Set up database

This project uses [MySQL](https://www.mysql.com) as a database.
Set up your MySQL database and make sure the service is running.
Go to `prisma/schema.prisma` and put in your MySQL credentials, the database name, and the name of the schema file.

Then migrate your database with [Lift](https://lift.prisma.io) by running:

```sh
prisma2 lift save --name 'init'
prisma2 lift up
```

### 4. Generate Photon (type-safe database client)

Run the following command to generate [Photon JS](https://photonjs.prisma.io/):

```sh
prisma2 generate
```

Now you can seed your database using the `seed` script from `package.json`:

```sh
npm run seed
```

### 5. Start the gRPC server

```sh
npm run server
```

The server is now running on `0.0.0.0:50051`.

### 6. Using the gRPC API

To use the gRPC API, you need a gRPC client.

```sh
npm run client
```

If you prefer a GUI client, try [BloomRPC](https://github.com/uw-labs/bloomrpc).

## Debugging

This is still a work in progress. If you run into problems, turn on debugging by setting:

```sh
export DEBUG=*
```

I am always happy to accept pull requests. :)
